import psycopg2

from typing import List
import logging

connection = psycopg2.connect(
    dbname='company',
    user='postgres',
    password='postgres',
    host='localhost',
)


class Salary:
    @staticmethod
    def add_salaries(ids: List[int], employee_ids: List[int], amounts: List[int]):
        cursor = connection.cursor()
        for (id, employee_id, amount) in zip(ids, employee_ids, amounts):
            if not id:      # there is no id
                connection.rollback()
                return Exception

            elif cursor.execute(f'SELECT id FROM Salary WHERE employee_id={employee_id} or id={id} LIMIT 1'): # if there exists such salary or employee
                connection.rollback()
                return Exception
            try:
                cursor.execute(f'INSERT INTO "Salary" VALUES ({id}, {employee_id}, {amount})')
                logging.info(f'Successfully inserted ({id}, {employee_id}, {amount}) into Salary')
            except Exception as e:
                logging.error(str(e))
                connection.rollback()
                return e

        connection.commit()
        cursor.close()


class Tests:
    @staticmethod
    def test_if_no_salary_id():
        Salary.add_salaries([None, None], [1, 2], [1000, 1000])

    @staticmethod
    def test_if_same_salary_id():
        Salary.add_salaries([1, 1], [1, 2], [1000, 1000])

    @staticmethod
    def test_if_same_employee_id():
        Salary.add_salaries([1, 2], [1, 1], [1000, 1000])


Tests.test_if_no_salary_id()
Tests.test_if_same_employee_id()
Tests.test_if_same_employee_id()
